## Mps-deploy 小程序批量发布助手

#### 对已有多个不同小程序实例运行同一套小程序源码情况，因当前的小程序类目限制或因某些原因无法使用微信第三方开放平台，那Mps-deploy可以帮你快速将更新的小程序（小游戏以及插件）代码一键批量的发布到对应的小程序实例，不必再开启微信开发者工具逐一修改发布。

比如有多个客户购买或使用了你开发的同一个小程序, 可能开发时没有使用微信开放平台或者你是个人开发者获取微信开放平台相关资质繁琐等原因无法使用微信开放平台, 那么mps-deploy能让你一次配置, 一键将版本批量更新到各个客户的小程序


![](https://img.shields.io/npm/v/mps-deploy?color=%2300a4ff) ![](https://img.shields.io/npm/dy/mps-deploy)

#### 安装

> 如果你的环境尚未安装mps-deploy的相关依赖，如babel，可按照提示信息安装对应的依赖
> "Couldn't find preset "es2015" relative to directory" --> npm install babel-preset-es2015 -g
> "Couldn't find preset "miniprogram-ci" relative to directory" --> npm install miniprogram-ci@1.0.51 -g

```javascript
npm i mps-deploy -g // 推荐使用cnpm i mps-deploy -g
```



#### 使用前提

> 秘钥文件：微信公众平台 —— 开发设置 —— 提交代码；
> 请在上文位置关闭白名单限制；
> 可能小程序开发者并非发布者，或发布者有多名，因此开发者需要在小程序项目的根目录中添加统一的版本信息文件: mps-deploy-version.json
```
// mps-deploy-version.json 格式如下
{
  "version": "0.0.1",
  "desc": "批量发布"
}
```

##### 使用

mps-deploy提供3种使用方式：命令行，GUI，部署到服务器(或接入到第三方程序)

##### 查看全部命令

```javascript
mps-deploy
```
![](/img/mps.png)
##### 命令行

```javascript
mps-deploy CMD "path/to/yourfile" // yourfile是一个发布JSON名单
```

```
{
  "project": "C:/Users/Administrator/Desktop/mps/weapp", // 小程序项目文件

  "type": "miniProgram", // miniProgram/miniProgramPlugin/miniGame/miniGamePlugin

  "es6": false, // 是否将es6编译为es5,提高兼容性

  "deploy": [ // 发布名单
​    {
​      "name": "app-one",
​      "appid": "wx00000000001",
​      "key": "C:/private.wx00000000001.key" // 秘钥文件，可从微信公众平台的开发设置中获得
​    },
​    {
​      "name": "app-two",
​      "appid": "wx00000000002",
​      "key": "C:/mps/private.wx00000000002.key"
​    }
  ]
}
```

##### GUI
执行下面的命令后会自动打开默认浏览器 (Windows系统)

```javascript
mps-deploy GUI 8181 // (自定义端口号)
```
![](/img/GUI-1.png)

![](/img/GUI-2.png)

Windows系统下CMD和GUI方式批量发布，mps-deploy会在C盘下创建"mps-deploy-log"文件夹，每次发布任务均会存储一份文本日志, Linux系统环境下暂未支持, **若未输出日志请查看本用户是否有对相关目录的读写权限**

##### 部署为服务 (或接入到第三方程序)

```javascript
mps-deploy SERVER 8181
```

mps-deploy会提供下列接口调用, 支持并发任务

```
// 提交发布任务
url: /postdata
method: post
headers: m-hash // String 当前发布用户的唯一标识 必填
data: [
  "response": {
    "host": "127.0.0.1",
    "port": "9090",
    "path": "/backdata"
  },
  "deployList": {
    "project": "C:/weapp", // 小程序项目路径
    "type": "miniProgram",
    "es6": true,
    "version": "0.0.1",
    "desc": "批量发布", // 版本描述
    "name": "app-one",
    "appid": "wx000000000001",
    "key": "C:/private.wx000000000001.key"
  }
]

// 获得发布进度结果方式--轮询mps-deploy提供的接口
// 本次发布任务结束之后mps-deploy只会将本次发布结果保存60s，轮询间隙不应大于60s，或根据你业务的需求修改相关源码
url: /result
method: get
headers: m-hash
response: {
    "status": "success",
    "type": "end", // 整个任务是否完成
    "data": [
        {
            "speed": "[1/1]",
            "appid": "wx000000000001",
            "status": "success",
            "time": "2020-07-22T09:30:37.235Z"
        }
    ]
}

// 获得发布进度结果方式--mps-deploy主动调用第三方提供的接口返回数据
url: "/yourApi" // 从"/postdata"接口中提交调用的第三方接口信息
method: "post"
data: {
    "type": "end",
    "m-hash": "xxxxxxx",
    "data": [
        {
            "speed": "[1/1]",
            "appid": "wx000000000001",
            "status": "success",
            "time": "2020-07-22T09:30:37.235Z"
        }
    ]
}

// 本地启动
node src/command.js server 8484
node src/command.js cmd 8484
node src/command.js gui 8484
```



#### 注意

> mps-deploy会对小程序项目做一些合法校验，但这是不完备的，因此强烈建议在批量发布之前先在微信开发者工具中上传一次，由官方开发工具校验本次构建的版本是否合法后再批量发布。
> 建议在代码开发完成时使用对应技术框架的cli，或微信开发者工具来编译es6语法；不建议使用mps-deploy将小程序项目中es6语法编译为es5





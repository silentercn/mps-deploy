const ci = require("miniprogram-ci");

class Ci {
  static deploy(params, current, fn) {
    params = params[current]
    return new Promise((reslove, reject) => {
    let project = new ci.Project({
      appid: params.appid,
      type: params.type,
      projectPath: params.project,
      privateKeyPath: params.key,
      ignores: ['node_modules/**/*'],
    })
    ci.upload({
      project,
      version: params.version,
      desc: params.desc,
      setting: {
        es6: params.es6,
      },
      onProgressUpdate: subPackageInfo => {
        fn(params, current, subPackageInfo)
      }
    })
    })
  }
}
module.exports = Ci;

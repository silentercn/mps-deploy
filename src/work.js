
const Ci = require(`./ci.js`);
let hash = null;
let deployList = null;

process.on("message", m => {
  if (m.hasOwnProperty('exit')) {
    process.exit()
  }
  deployList = m.deployList;
  hash = m.hash;
  start(deployList);
});

let start = (deployList) => {
  let fn = (deploy, current, e) => {
    let Info = e.toString();
    let date = new Date();
    if (Info.indexOf("done: upload") === 0) {
      process.send({
        hash,
        data: {
          speed: `[${current + 1}/${deployList.length}]`,
          appid: deploy.appid,
          status: "finished",
          time: date,
        },
      });
      if (current < deployList.length - 1) {
        Ci.deploy(deployList, ++current, fn);
      }
    }
  };
  Ci.deploy(deployList, 0, fn);
};

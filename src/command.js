#!/usr/bin/env node
const cmd = require(`./cmd.js`);
const gui = require(`./gui.js`);
const server = require(`./server.js`);
console.log("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓");
console.log("┃ Welcome to use mps-deploy!                          ┃");
console.log("┃ ----                                                ┃");
console.log("┃ version: 0.1.3                                      ┃");
console.log("┃ document on npm: www.npmjs.com/package/mps-deploy   ┃");
console.log("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛");

const [node, path, ...argv] = process.argv;
// console.log(path, 'path')
if (argv.length === 0) {
  console.log("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
  console.log(' Please enter parameters after the command "mps-deploy');
  console.log(' Use CMD : mps-deploy CMD "path/to/yourfile"');
  console.log(" Use GUI : mps-deploy GUI 8181(port)");
  console.log(" Use SERVER : mps-deploy SERVER 8181(port)");
  console.log("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
}

if (argv[0] === "CMD" || argv[0] === "cmd") {
  cmd(argv)
}

if (argv[0] === "GUI" || argv[0] === "gui") {
  if (Number(argv[1]).constructor !== Number) {
    console.log("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
    console.log("Please enter the correct port");
    console.log("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
    return;
  }
  gui(argv[1])
}

if (argv[0] === "SERVER" || argv[0] === "server") {
  if (Number(argv[1]).constructor !== Number) {
    console.log("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
    console.log("Please enter the correct port");
    console.log("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━");
    return;
  }
  server(argv[1])
}

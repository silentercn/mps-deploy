 
/*
* Mps-deploy v0.1.3
* Author: Leeeink 
* License: MIT
* Repository: https://gitee.com/leeeink/mps-deploy
*/
module.exports = (function () {
  console.log("┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓");
  console.log("┃ Welcome to use mps-deploy!                          ┃");
  console.log("┃ ----                                                ┃");
  console.log("┃ version: 0.0.4                                      ┃");
  console.log("┃ document on npm  : www.npmjs.com/package/mps-deploy ┃");
  console.log("┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛");
})()
